import java.util.Scanner;

public class Shop{
	public static void main(String[] args){
    Scanner reader = new Scanner(System.in);
    Boba[] customer = new Boba[4];
    
    for (int i = 0; i < customer.length; i++){
      //Welcome Message + Menu
      System.out.println("Welcome to the boba shop! Here's some specifications: ");
      System.out.println("Toppings: tapioca, grass jelly (only applicable for milk tea), lychee jelly (only applicable for fruit juice) and none");
      System.out.println("Flavour: tea (only applicable for milk base) and fruit");
      
	  String newBase;
	  String newFlavour;
	  String newToppings;
	  int newSweet;
	  
	  //User inputs
      System.out.println("Would you like juice or milk base? (ex: milk/juice)");
	  newBase = reader.nextLine();
      System.out.println("What fruit flavour would you like (ex: strawberry/honeydew/mango)? If not, just write: tea (tea is only applicable to a milk base)");
	  newFlavour = reader.nextLine();
      System.out.println("Which topping would you like? (ex: tapioca/grass jelly/lychee jelly/none)");
      newToppings = reader.nextLine();
      System.out.println("What percentage or sugar would you like? (ex: 30, 50, 75, 100, 125)");
      newSweet = Integer.parseInt(reader.nextLine());
	  
	  customer[i] = new Boba(newBase, newFlavour, newToppings, newSweet);
    }
	
	System.out.println("Base: " + customer[customer.length - 1].getBase());
    System.out.println("Flavour: " + customer[customer.length - 1].getFlavour());
    System.out.println("Toppings: " + customer[customer.length - 1].getToppings());
    System.out.println("Sweet %: " + customer[customer.length - 1].getSweet());
    System.out.println("---");
    customer[customer.length - 1].drink(customer[customer.length - 1].getBase(), customer[customer.length - 1].getFlavour(), customer[customer.length - 1].getToppings());
    
	System.out.println("Would you like juice or milk base? (ex: milk/juice)");
    customer[customer.length - 1].setBase(reader.nextLine());
	System.out.println("What fruit flavour would you like (ex: strawberry/honeydew/mango)? If not, just write: tea (tea is only applicable to a milk base)");
    customer[customer.length - 1].setFlavour(reader.nextLine());
	System.out.println("Which topping would you like? (ex: tapioca/grass jelly/lychee jelly/none)");
    customer[customer.length - 1].setToppings(reader.nextLine());
	
    System.out.println("Base: " + customer[customer.length - 1].getBase());
    System.out.println("Flavour: " + customer[customer.length - 1].getFlavour());
    System.out.println("Toppings: " + customer[customer.length - 1].getToppings());
    System.out.println("Sweet %: " + customer[customer.length - 1].getSweet());
    System.out.println("---");
    customer[customer.length - 1].drink(customer[customer.length - 1].getBase(), customer[customer.length - 1].getFlavour(), customer[customer.length - 1].getToppings());
  }
}